// required for heroku staging
enablePlugins(JavaAppPackaging)

// basic info
name := "scala-calc"
version := "1.0"
scalaVersion := "2.12.1"

// required for naive-http server
resolvers += "Tim Tennant's repo" at "http://dl.bintray.com/timt/repo/"
libraryDependencies += "io.shaka" %% "naive-http" % "91"

// required for scalatest
resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
