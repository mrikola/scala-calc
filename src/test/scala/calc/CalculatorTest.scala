package calc

import org.scalatest.{Assertions, FunSuite}

class CalculatorTest extends FunSuite {

  test("integer") {
    val result = new Calculator("1234").calculate()
    Assertions.assert(result == 1234)
  }

  test("decimal") {
    val result = new Calculator("12.34").calculate()
    Assertions.assert(result == 12.34)
  }

  test("addition") {
    val result = new Calculator("1+2+3+4").calculate()
    Assertions.assert(result == 10)
  }

  test("subtraction") {
    val result = new Calculator("7-3-4").calculate()
    Assertions.assert(result == 0)
  }

  test("multiplication") {
    val result = new Calculator("4*6*5").calculate()
    Assertions.assert(result == 120)
  }

  test("division") {
    val result = new Calculator("20/5/2").calculate()
    Assertions.assert(result == 2)
  }

  test("precedence") {
    val result = new Calculator("1+2*3-4/2").calculate()
    Assertions.assert(result == 5)
  }

  test("parentheses") {
    val result = new Calculator("1+2*(3-4)/2").calculate()
    Assertions.assert(result == 0)
  }

  test("whitespace") {
    val result = new Calculator("1 + 2 *       (3 -4) / 2").calculate()
    Assertions.assert(result == 0)
  }

  test("nested parentheses") {
    val result = new Calculator("1+2*((9+5)/2+(3-4)*5)").calculate()
    Assertions.assert(result == (1+2*((9+5)/2+(3-4)*5)))
  }

  test("negative number") {
    val result = new Calculator("2 + -5").calculate()
    Assertions.assert(result == -3)
  }

  test("negative expression") {
    val result = new Calculator("-(4+9)").calculate()
    Assertions.assert(result == -13)
  }

  test("error: nothing but whitespace") {
    assertThrows[IllegalArgumentException] {
      new Calculator("   ").calculate()
    }
  }

  test("error: unknown symbol") {
    assertThrows[IllegalArgumentException] {
      new Calculator("herpderp").calculate()
    }
  }

  test("error: lone decimal point") {
    assertThrows[IllegalArgumentException] {
      new Calculator("2 + . + 5").calculate()
    }
    assertThrows[IllegalArgumentException] {
      new Calculator("2 + -. + 5").calculate()
    }
  }

  test("error: two decimal points") {
    assertThrows[IllegalArgumentException] {
      new Calculator("2.5.4").calculate()
    }
  }

  test("error: missing operator") {
    assertThrows[IllegalArgumentException] {
      new Calculator("2 5+4").calculate()
    }
  }

  test("error: missing operand") {
    assertThrows[IllegalArgumentException] {
      new Calculator("/5+4").calculate()
    }
    assertThrows[IllegalArgumentException] {
      new Calculator("5+4/").calculate()
    }
    assertThrows[IllegalArgumentException] {
      new Calculator("5+/4").calculate()
    }
  }

  test("error: division by zero") {
    assertThrows[IllegalArgumentException] {
      new Calculator("5+4/(5-5)").calculate()
    }
  }

  test("error: missing left parenthesis") {
    assertThrows[IllegalArgumentException] {
      new Calculator("5+4/5-5)").calculate()
    }
  }

  test("error: missing right parenthesis") {
    assertThrows[IllegalArgumentException] {
      new Calculator("5+4/(5-5").calculate()
    }
  }
}


