package calc

import java.util.Base64

import io.shaka.http.ContentType.APPLICATION_JSON
import io.shaka.http.HttpServer
import io.shaka.http.Request.{GET, POST}
import io.shaka.http.RequestMatching._
import io.shaka.http.Response.respond
import io.shaka.http.StaticResponse.static

object Main extends App {
  val SERVER_PORT : String = scala.util.Properties.envOrElse("PORT", "8080")
  val httpServer : HttpServer = HttpServer(SERVER_PORT.toInt).handler{
    case GET(url"/calculus?query=$query") =>
      try {
        val decoded = new String(Base64.getDecoder.decode(s"$query"))
        val result = new Calculator(decoded).calculate()
        respond(s"""{"error": false, "result":"$result"}""").contentType(APPLICATION_JSON)
      } catch {
        case e: Exception => val msg = e.getMessage; respond(s"""{"error": true, "message":"$msg"}""").contentType(APPLICATION_JSON)
      }
    case GET(url"/") => static("ui/", "index.html")
    case GET(path) => static("ui/", path)
    case POST(_) => respond("""{"error": true, "message": "POST requests are not supported"}""").contentType(APPLICATION_JSON)
  }.start()
}
