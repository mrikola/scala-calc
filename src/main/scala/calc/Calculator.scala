package calc

import scala.collection.mutable


class Calculator (equation: String) {

  // enum for supported token types
  object TokenTypes extends Enumeration {
    val Whitespace, Number, Add, Subtract, Multiply, Divide, LeftParen, RightParen, Error = Value
  }

  // a few convenience methods
  private def isAddOrSubtract(token: TokenTypes.Value): Boolean = token match {
    case TokenTypes.Add | TokenTypes.Subtract => true
    case _ => false
  }

  private def isDivideOrMultiply(token: TokenTypes.Value): Boolean = token match {
    case TokenTypes.Divide | TokenTypes.Multiply => true
    case _ => false
  }

  private def isDecimalPoint(char: Char):Boolean = char == '.'


  private def detectTokenType(char: Char, lastTokenType:TokenTypes.Value):TokenTypes.Value = {
    char match {
      case '+' => TokenTypes.Add
      case '-' =>
        // the minus sign is problematic: it can mean a binary subtract operator or unary negation..
        // further, the unary negation works for parenthesized expressions in addition to numbers
        // we try to cope with both of these cases

        // if the previous token was the end of another expression (a number or right parenthesis)
        // then the minus represents a subtraction
        // otherwise it's the start of a negative number
        if (lastTokenType == TokenTypes.Number || lastTokenType == TokenTypes.RightParen)
          TokenTypes.Subtract
        else
          TokenTypes.Number
      case '*' => TokenTypes.Multiply
      case '/' => TokenTypes.Divide
      case ' ' => TokenTypes.Whitespace
      case '(' => TokenTypes.LeftParen
      case ')' => TokenTypes.RightParen

      // the decimal point is considered a numeric character
      case '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '.' => TokenTypes.Number
      case _ => TokenTypes.Error
    }
  }


  private def tokenize(input: String): List[(TokenTypes.Value, Double)] = {
    var i:Int = 0
    var lastTokenType:TokenTypes.Value = TokenTypes.Error
    var result:mutable.ListBuffer[(TokenTypes.Value, Double)] = mutable.ListBuffer()

    while (i < input.length) {
      val currentCharacter:Char = input.charAt(i)
      val currentTokenType:TokenTypes.Value = detectTokenType(currentCharacter, lastTokenType)
      var currentToken:String = currentCharacter.toString

      currentTokenType match {
        // unrecognized symbols result in exception
        case TokenTypes.Error => throw new IllegalArgumentException("Unrecognized symbol: " + currentToken)

        // whitespace is trimmed out at this point
        case TokenTypes.Whitespace => {}

        case TokenTypes.Number => {
          // numbers can span multiple characters, so we keep peeking at the next character until non-number or EOF is found
          var decimalPointFound:Boolean = isDecimalPoint(currentCharacter)
          while (i + 1 < input.length && detectTokenType(input.charAt(i + 1), TokenTypes.Number) == TokenTypes.Number) {
            val next = input.charAt(i + 1)
            if (decimalPointFound && isDecimalPoint(next)) throw new IllegalArgumentException("Two decimal points")
            if (isDecimalPoint(next )) decimalPointFound = true
            currentToken += next
            i += 1
          }

          // special case 1: nothing but a decimal point
          if (currentToken.contentEquals(".") || currentToken.contentEquals("-."))
            throw new IllegalArgumentException("Decimal point without integer or decimal part")

          // special case 2: in case of a lonesome minus sign that looks like a number, interpret it as multiplication by -1
          if (currentToken.contentEquals("-")) {
            result += ((TokenTypes.Number, -1))
            result += ((TokenTypes.Multiply, Double.NaN))
          } else {
            // otherwise interpret the number as is
            result += ((currentTokenType, currentToken.toDouble))
          }

          lastTokenType = currentTokenType
        }

        // other symbols are operators and parentheses that require no further parsing
        case _ => {
          lastTokenType = currentTokenType
          result += ((currentTokenType, Double.NaN))
        }
      }
      i += 1
    }
    if (result.isEmpty) throw new IllegalArgumentException("Please enter a formula")
    result.toList
  }

  // Dijkstra's shunting-yard algorithm to convert to reverse polish notation
  // source: https://en.wikipedia.org/wiki/Shunting-yard_algorithm

  private def shunt(tokens: List[(TokenTypes.Value, Double)]) :List[(TokenTypes.Value, Double)] = {

    // we have two data structures in addition to the source list
    // the operator stack where mathematical operators are pushed and popped in LIFO fashion
    // and the result queue which is the resulting RPN equation

    var result:mutable.ListBuffer[(TokenTypes.Value, Double)] = mutable.ListBuffer()
    var stack:mutable.ListBuffer[(TokenTypes.Value, Double)] = mutable.ListBuffer()
    var previousTokenType:TokenTypes.Value = TokenTypes.Error

    tokens.foreach(f = (token) => {
      val currentTokenType = token._1
      currentTokenType match {

        // numbers are pushed directly into the result queue
        case TokenTypes.Number => {
          if (previousTokenType == TokenTypes.Number) throw new IllegalArgumentException("Two consecutive numbers")
          result += token
        }

        // left parenthesis is simply added to the operator stack
        case TokenTypes.LeftParen => stack += token

        case TokenTypes.RightParen => {
          // purge the stack until we find the matching left parenthesis
          while (stack.nonEmpty && stack.last._1 != TokenTypes.LeftParen) {
            result += stack.last // move top element to result queue
            stack = stack.init // then remove it
          }

          // if the stack empties before we find the (, we have a mismatch
          if (stack.isEmpty)
            throw new IllegalArgumentException("Missing left parenthesis")
          else
            stack = stack.init // remove the left parenthesis
        }

        // operators are only put on stack in a position where they have the highest distinct precedence
        // iow. * and / are higher than + and -
        case TokenTypes.Add | TokenTypes.Subtract => {
          // flush everything with higher or equal precedence from the stack
          while (stack.nonEmpty && (isAddOrSubtract(stack.last._1) || isDivideOrMultiply(stack.last._1) )) {
            result += stack.last
            stack = stack.init
          }
          // then put the token on stack
          stack += token
        }
        case TokenTypes.Multiply | TokenTypes.Divide => {
          // again, flush everything with higher or equal precedence
          while (stack.nonEmpty && isDivideOrMultiply(stack.last._1)) {
            result += stack.last
            stack = stack.init
          }
          stack += token
        }
        case _ => throw new IllegalArgumentException("Unknown token")
      }
      previousTokenType = currentTokenType
    })

    // finally, all the remaining operators on the stack are handled
    stack.reverse.foreach(f = (operator) => {
      operator._1 match {
        // any parentheses at this point are unmatched
        case TokenTypes.LeftParen => throw new IllegalArgumentException("Missing right parenthesis")
        case TokenTypes.RightParen => throw new IllegalArgumentException("Missing left parenthesis")

        // other operators are just flushed to the queue
        case _ => result += operator
      }
    })
    result.toList
  }

  def calculate(): Double = {
    // first convert the calculation into reverse polish notation
    val rpn = shunt(tokenize(equation))
    var stack:mutable.ListBuffer[Double] = mutable.ListBuffer()

    // then do the actual calculation
    rpn.foreach(f = (token) => {
      token._1 match {
        // if the token is a number, put its numeric value into the calculation stack
        case TokenTypes.Number => stack += token._2

        case otherType => {
          // if the token isn't a number, it's an operator
          // all our operators are binary, thankfully
          if (stack.size < 2)
            throw new IllegalArgumentException(s"$otherType operator didn't have two operands")

          // pop the top two numbers off the stack
          val right = stack.last
          val left = stack.init.last
          stack = stack.init.init

          // therefore we need to have two numeric operands.. (this should never happen)
          if (left == Double.NaN || right == Double.NaN) {
            throw new IllegalArgumentException(s"$otherType operator didn't have two numeric operands")
          }

          // if we do, do the calculation and return the result back to the stack
          otherType match {
            case TokenTypes.Add => stack += (left + right)
            case TokenTypes.Subtract => stack += (left - right)
            case TokenTypes.Multiply => stack += (left * right)
            case TokenTypes.Divide => if (right == 0) throw new IllegalArgumentException("Divide by zero") else stack += (left / right)
            case _ => throw new IllegalArgumentException("Unknown operator")
          }
        }
      }
    })

    if (stack.size == 1)
      stack.head
    else
      throw new IllegalArgumentException("stack size was not 1 at the end")
  }
}